﻿using System;
using System.Collections.Generic;

public class OurMessenger
{
// 	// Standard singleton pattern
// 	static OurMessenger			sInstance;
// 	public static OurMessenger	Instance { get { if (sInstance == null) { sInstance = new OurMessenger(); } return sInstance; } }

	// Simplified singleton pattern
	static OurMessenger			sInstance = new OurMessenger();
	public static OurMessenger	Instance { get { return sInstance; } }

	// This is the template for the callback we mustc support
	public delegate void		Callback(int MessageIdent, object Sender, object OptionalValue);

	// List of callbacks
	List<Callback>				m_MessageCallbackList = new List<Callback>(10);

	// Make sure constructor is private so nothing else can allocate one
	private OurMessenger()
	{
	}

	internal void AddToMessageQueue(Callback NewCallback)
	{
		// Check to see we're not already added this delegate
		if (m_MessageCallbackList .IndexOf(NewCallback) == -1)
			m_MessageCallbackList .Add(NewCallback);
	}

	internal void Send(int MessageIdent, object Sender = null, object OptionalValue = null)
	{
		// NOTE: cannot use a foreach here as this callback could setup other callbacks
		for (int loop = 0; loop < m_MessageCallbackList.Count; loop++)
		{
			m_MessageCallbackList[loop](MessageIdent, Sender, OptionalValue);
		}
	}
}
