﻿using UnityEngine;
using System.Collections;

public class rotate : MonoBehaviour {
    public float speed = 40f;
    public float stop = 0f;
 
    public Transform objectToRotate; 
    // Use this for initialization
      
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        if(speed != 0)
        objectToRotate.Rotate(Vector3.up, speed * Time.deltaTime);

    }

    void OnTriggerEnter(Collider other)
    {
        speed = 0f;
      //  objectToRotate.Rotate(Vector3.up, stop * Time.deltaTime);
    }

    void OnTriggerExit(Collider other)
    {

        speed = 40f;
       // objectToRotate.Rotate(Vector3.up, speed * Time.deltaTime);

    }
}
