﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class finish : MonoBehaviour {

    
    public ParticleSystem.EmissionModule FINISH;

    private bool m_player1End = false;
    private bool m_player2End = false;

    private bool m_endTriggered = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(m_player1End && m_player2End && !m_endTriggered)
        {
           
            FINISH.enabled = true;
            Invoke("TurnOff", 10f);
            m_endTriggered = true;
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")   //Later will add for both players
        {
            m_player1End = true;

        }

        if (collider.tag == "Player2")   //Later will add for both players
        {
            m_player2End = true;

        }

        if (collider.tag == "Player" && collider.tag == "Player2")   //Later will add for both players
        {
            FINISH.enabled = true;
            Invoke("TurnOff", 10f);

        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")   //Later will add for both players
        {
            m_player1End = false;

        }

        if (collider.tag == "Player2")   //Later will add for both players
        {
            m_player2End = false;

        }

        if (collider.tag == "Player" && collider.tag == "Player2")   //Later will add for both players
        {
            FINISH.enabled = true;
            Invoke("TurnOff", 10f);

        }
    }
    //void OnTriggetStay(Collider collider)
    //{

    //    if (collider.tag == "Player" && collider.tag == "Player2")   //Later will add for both players
    //    {
    //        FINISH.enableEmission = true;
    //        Invoke("TurnOff", 10f);

    //    }


    //}

    private void TurnOff()
    {
       
        FINISH.enabled = false;
        Invoke("BackMenu", 10f);
      

    }

    private void BackMenu()
    {
  SceneManager.LoadScene(0);
    }

}
