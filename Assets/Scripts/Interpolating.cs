﻿ using UnityEngine;
using System.Collections;

public class Interpolating : MonoBehaviour 
{
    public Transform objectToMove;
    public Transform destinationObject;

    private float timeToInterpolate = 1.0f;

  //  private Transform m_transform;

    [SerializeField]
    private float interpVal = 0.0f;


    private Vector3 startPosition;
	// Use this for initialization
	void Start () 
    {
       // m_transform = transform;
        startPosition = objectToMove.position;
       
	}

    private void StartSimulation()
    {
        StartCoroutine(StartCounter());
    }

    private void EndSimulation()
    {
        StartCoroutine(EndCounter());
    }

	void  OnTriggerEnter(Collider other)
    {
            
        Invoke("StartSimulation", 0.0f);
    }

    void OnTriggerExit(Collider other)
    {
        //print("i exists!");
        Invoke("EndSimulation", 0.0f);
        
    }




    //// Update is called once per frame
    void Update() 
    {
        //put aa box colider no mesh/render on box
        //on trigger enter
    


    }

    private IEnumerator StartCounter()
    {
        float timeToFinishMove = 0.0f;

        while (interpVal <= 1)
        {
            timeToFinishMove += Time.deltaTime;

            interpVal = timeToFinishMove / timeToInterpolate;

            objectToMove.position = Vector3.Lerp(startPosition, destinationObject.position, interpVal);

            yield return null;
        }

        interpVal = 0f;
    }

    private IEnumerator EndCounter()
    {
        
        float timeToFinishMove = 0.0f;

        while (interpVal <= 1)
        {
            timeToFinishMove += Time.deltaTime;

            interpVal = timeToFinishMove / timeToInterpolate;

            objectToMove.position = Vector3.Lerp(objectToMove.position, startPosition, interpVal);

            yield return null;

            
        }

        interpVal = 0f;
    }




}
